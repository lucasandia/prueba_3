# Generated by Django 3.1.3 on 2020-12-02 02:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appgestion', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='casa',
            name='cocina',
            field=models.CharField(max_length=2),
        ),
        migrations.AlterField(
            model_name='casa',
            name='living_comedor',
            field=models.CharField(max_length=2),
        ),
    ]

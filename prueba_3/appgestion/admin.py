from django.contrib import admin

# Register your models here.
from appgestion.models import Casa,Solicitud_Contacto

admin.site.register(Casa)
admin.site.register(Solicitud_Contacto)
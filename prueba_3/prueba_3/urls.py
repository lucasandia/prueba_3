"""prueba_3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from prueba_3.Views_modelo_a_medida import funcion_casa_a_medida
from prueba_3.views_modelo_por_metraje import funcion_casa_a_metraje
from appgestion import views
from appgestion import views_eliminar

urlpatterns = [
    path('',views.portada),
    path('admin/', admin.site.urls),
    path('portada/',views.portada),
    path('quienes_somos/',views.quienes_somos),
    path('buscar_por_cant_habitaciones/',views.buscar_por_cant_habitaciones),
    path('contacto/',views.contacto),
    path('galeria/',views.galeria),
    path('metraje/<int:metros_cuadrados>',funcion_casa_a_metraje),
    path('medida/<int:numero_banios>/<int:numero_habitaciones>',funcion_casa_a_medida),
    path('ingresar_solicitud/',views.ingresar_solicitud),
    path('buscar_sol_contacto/',views.buscar_sol_contacto),
    path('buscar_sol_por_email/',views.buscar_sol_por_email),
    path('buscar_sol_por_comuna/',views.buscar_sol_por_comuna),
    path('formulario_eliminar/',views.formulario_eliminar),
    path('eliminar_solicitud_por_id/',views_eliminar.eliminar_solicitud_por_id),
]
